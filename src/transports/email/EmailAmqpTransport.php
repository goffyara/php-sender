<?php
namespace bb\sender\transports\email;

use bb\sender\transports\AbstractAmqpTransport;

class EmailAmqpTransport extends AbstractAmqpTransport
{
    public $queue = 'email';

    public function getMessageData($Email)
    {
        return [
            "app_group_id" => 1,
            "app_id" =>  1,
            "params" => ['is_test' => $Email->isTest()],
            "email" =>  $Email->getEmail(),
            "subject" => $Email->getSubject(),
            "content" => $Email->getContent(),
        ];
    }
}