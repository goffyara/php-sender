<?php
namespace bb\sender\transports\sms;

use bb\sender\transports\AbstractAmqpTransport;

class SmsAmqpTransport extends AbstractAmqpTransport
{
    public $queue = 'sms';

    public function getMessageData($Sms)
    {
        return [
            "app_group_id" => $Sms->getAppGroupId(),
            "app_id" =>  $this->getAppId(),
            "params" => ['is_test' => $Sms->isTest()],
            "phone" =>  $Sms->getPhone(),
            "text" => $Sms->getText(),
        ];
    }
}