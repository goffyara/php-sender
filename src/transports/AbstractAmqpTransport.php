<?php
namespace bb\sender\transports;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use PhpAmqpLib\Wire\AMQPTable;
use bb\sender\messages\MessageInterface;

abstract class AbstractAmqpTransport
{
    private $_channel;
    private $_connection;
    private $_message;
    private $_app_id;

    protected $queueDeclared;
    protected $queue;

    public $messageConfig = [
        'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT
    ];

    private $host;
    private $port;
    private $user;
    private $password;
    private $vhost;

    function __construct(array $config) {

        if (!isset($config['host'])) {
            throw new \Exception("host is require in transport config array");
        }

        if (!isset($config['port'])) {
            throw new \Exception("port is require in transport config arrray");
        }

        $this->app_id = $config['app_id'];
        $this->host = $config['host'];
        $this->port = $config['port'];
        $this->user = $config['user'] ?? 'guest';
        $this->password = $config['password'] ?? 'guest';
        $this->vhost = $config['vhost'] ?? 'sender';
    }

    public function send($message)
    {
        $data = $this->getMessageData($message);
        $data = json_encode($data, 320);

        $Message = $this->getMessage()->setBody($data);
        $this->sendMessage($Message);
    }

    abstract public function getMessageData($message);

    public function getAppId()
    {
        return $this->app_id;
    }

    /**
     * отправка в очередь
     * @return void
     */
    public function sendMessage(AMQPMessage $Message)
    {
        $this->declareQueue();
        $this->getChannel()->basic_publish($Message, '', $this->queue);
    }

    /**
     * Создание очереди, если необходимо
     * @return \PhpAmqpLib\Message\AMQPMessage
     */
    public function declareQueue()
    {
        if ($this->queueDeclared) {
            return;
        }

        $args = [
            'x-max-priority' => 10
        ];

        $this->getChannel()->queue_declare($this->queue, false, true, false, false, false, new AMQPTable($args));
        $this->queueDeclared = true;
    }

    /**
     * @throws \PhpAmqpLib\Exception\AMQPProtocolConnectionException
     * @return \PhpAmqpLib\Channel\AMQPChannel
     */
    public function getChannel() {

        if (!$this->getConnection()->isConnected()) {
            $this->getConnection()->reconnect();
        }

        if (is_object($this->_channel)) {
            return $this->_channel;
        }

        $this->_channel = $this->getConnection()->channel();
        return $this->_channel;
    }

    /**
     * @throws \PhpAmqpLib\Exception\AMQPProtocolConnectionException
     * @return \PhpAmqpLib\Connection\AMQPStreamConnection
     */
    private function getConnection() {

        if (is_object($this->_connection)) {
            return $this->_connection;
        }

        $this->_connection = new AMQPStreamConnection(
            $this->host,
            $this->port,
            $this->user,
            $this->password,
            $this->vhost
        );

        return $this->_connection;
    }

    /**
     * @return \PhpAmqpLib\Message\AMQPMessage
     */
    public function getMessage()
    {
        if (is_object($this->_message)) {
            return $this->_message;
        }

        $this->_message = new AMQPMessage('', $this->messageConfig);

        return $this->_message;
    }
}