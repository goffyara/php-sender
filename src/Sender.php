<?php
namespace bb\sender;

use bb\sender\messages\MessageFactory;
use bb\sender\transports\sms\SmsAmqpTransport;
use bb\sender\transports\email\EmailAmqpTransport;

class Sender
{
    private $smsTransport;
    private $emailTransport;

    function __construct($config = [])
    {
        $this->smsTransport = new SmsAmqpTransport($config);
        $this->emailTransport = new EmailAmqpTransport($config);
    }

    /**
     * @return \bb\sender\messages\sms\Sms
     */
    public function getSms()
    {
        return MessageFactory::create('sms', $this->smsTransport);
    }

    /**
     * @return \bb\sender\messages\email\Email
     */
    public function getEmail()
    {
        return MessageFactory::create('email', $this->emailTransport);
    }

    /**
     * @return \bb\sender\transports\sms\SmsAmqpTransport
     */
    public function getSmsTransport()
    {
        return $this->smsTransport;
    }

    /**
     * @return \bb\sender\transports\email\EmailAmqpTransport
     */
    public function getEmailTransport()
    {
        return $this->emailTransport;
    }
}
