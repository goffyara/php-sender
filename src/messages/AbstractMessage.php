<?php
namespace bb\sender\messages;

abstract class AbstractMessage
{
    const SMS = 'sms';
    const EMAIL = 'email';

    protected $type;
    protected $transport;
    private $priority;
    private $app_group_id;
    private $is_test = false;

    public function getPriority()
    {
        return $this->priority;
    }

    public function setPriority($priority)
    {
        $this->priority = $priority;
        return $this;
    }

    public function getAppGroupId()
    {
        return $this->app_group_id;
    }

    public function setAppGroupId($id)
    {
        $this->app_group_id = $id;
        return $this;
    }

    public function setIsTest($isTest = true)
    {
        $this->is_test = (bool) $isTest;
        return $this;
    }

    public function isTest()
    {
        return $this->is_test;
    }

    public function send()
    {
        return $this->transport->send($this);
    }
}