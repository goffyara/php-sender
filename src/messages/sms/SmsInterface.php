<?php
namespace bb\sender\messages\sms;

interface SmsInterface
{
    /**
     * @param string $text
     */
    public function getText();

    /**
     * @param string $text
     * @return self reference.
     */
    public function setText($text);

    /**
     * @return string
     */
    public function getPhone();

    /**
     * @param string $phone
     * @return $this self reference.
     */
    public function setPhone($phone);
}