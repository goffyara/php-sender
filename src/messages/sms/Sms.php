<?php
namespace bb\sender\messages\sms;

use bb\sender\messages\MessageInterface;
use bb\sender\messages\AbstractMessage;
use bb\sender\transport\TransportInterface;
/**
*
*/
class Sms extends AbstractMessage implements SmsInterface
{
    private $text;
    private $phone;

    public $transport;

    function __construct($transport)
    {
        $this->transport = $transport;
        $this->type = self::SMS;
    }

    public function send()
    {
        return $this->transport->send($this);
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    public function getText()
    {
        return $this->text;
    }

    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }


}