<?php
namespace bb\sender\messages\email;

use bb\sender\messages\AbstractMessage;

class Email extends AbstractMessage implements EmailInterface
{
    private $content;
    private $subject;
    private $email;

    public $transport;

    function __construct($transport)
    {
        $this->transport = $transport;
        $this->type = self::EMAIL;
    }

    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function getSubject()
    {
        return $this->subject;
    }

    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }
}