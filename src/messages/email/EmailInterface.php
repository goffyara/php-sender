<?php
namespace bb\sender\messages\email;

interface EmailInterface
{
    /**
     * @return string
     */
    public function getContent();

    /**
     * @param string $content
     * @return self reference.
     */
    public function setContent($content);

    /**
     * @return string
     */
    public function getSubject();

    /**
     * @param string $subject
     * @return self reference.
     */
    public function setSubject($subject);

    /**
     * @return string
     */
    public function getEmail();

    /**
     * @param string $email
     * @return self reference.
     */
    public function setEmail($email);
}