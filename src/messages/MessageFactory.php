<?php
namespace bb\sender\messages;

use bb\sender\messages\email\Email;
use bb\sender\messages\sms\Sms;

class MessageFactory
{
    public static function create($type, $transport)
    {
        if ($type == 'email') {
            return new Email($transport);
        }
        if ($type == 'sms') {
            return new Sms($transport);
        }

        throw new \InvalidArgumentException('Неизвестный формат');
    }
}