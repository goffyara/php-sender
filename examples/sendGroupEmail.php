<?php
use bb\sender\Sender;

require __DIR__. '/../vendor/autoload.php';

$config = [
    'app_id' => 1,
    'host' => '127.0.0.1',
    'port' => '5672',
];

$Sender = new Sender($config);

$html = '<body><h1>Test</h1><body>';

$Email = $Sender
    ->getEmail()
    ->setPriority(5)
    ->setSubject('test')
    ->setIsTest()
    ->setAppGroupId(2);

$time_start = microtime(true);

$count = 300000;

for ($i=0; $i < $count; $i++) {
    $Email
        ->setEmail($i . '@test.test')
        ->setContent('<body><h1>Test' . $i . '</h1><body>')
        ->send();
}

$time_end = microtime(true);
$time = $time_end - $time_start;

echo 'Память -- ' . memory_get_peak_usage(true) . PHP_EOL;
echo 'Общее время -- ' . $time . PHP_EOL;
echo 'Одно -- ' . $time/($count-1) . PHP_EOL;
echo 'В секунду -- ' . ($count-1)/$time . PHP_EOL;

$Sender->getEmailTransport()->getChannel()->queue_purge('email');

die();