<?php
use bb\sender\Sender;

require __DIR__. '/../vendor/autoload.php';

$config = [
    'app_id' => 1,
    'host' => '127.0.0.1',
    'port' => '5672',
];

$Sender = new Sender($config);

$Sms = $Sender
    ->getSms()
    ->setText('tdfg')
    ->setIsTest()
    ->setPriority(5)
    ->setAppGroupId(2);

$time_start = microtime(true);

$count = 300000;

for ($i=0; $i < $count; $i++) {
    $Sms->setPhone($i)->send();
}

$time_end = microtime(true);
$time = $time_end - $time_start;

echo 'Память -- ' . memory_get_peak_usage(true) . PHP_EOL;
echo 'Общее время -- ' . $time . PHP_EOL;
echo 'Одно -- ' . $time/($count-1) . PHP_EOL;
echo 'В секунду -- ' . ($count-1)/$time . PHP_EOL;

$Sender->getSmsTransport()->getChannel()->queue_purge('sms');

die();