<?php

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Wire\AMQPTable;
use PhpAmqpLib\Message\AMQPMessage;

require __DIR__. '/../vendor/autoload.php';


$connection = new AMQPStreamConnection(
    '127.0.0.1',
    '5672',
    'guest',
    'guest',
    'sender'
);

$channel = $connection->channel();
$args = [
    'x-max-priority' => 10
];

$channel->queue_declare('email', false, true, false, false, false, new AMQPTable($args));

$time_start = microtime(true);

$count = 300000;

$Message = new AMQPMessage('', ['delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT]);

for ($i=0; $i < $count; $i++) {

    $template = [
        "app_group_id" => 1,
        "app_id" =>  1,
        "params" => ['is_test' => true],
        "email" =>  $i . '@test.test',
        "subject" => 'test',
        "content" => '<body><h1>Test' . $i . '</h1><body>',
    ];


    $Message->setBody(json_encode($template, 320));
    $channel->basic_publish($Message, '', 'email');

    $template = [
        "app_group_id" => 2,
        "app_id" =>  1,
        "params" => ["is_test" => true],
        "phone" =>  $i,
        "text" => "test2"
    ];


    $Message->setBody(json_encode($template, 320));
    $channel->basic_publish($Message, '', 'sms');
}

$time_end = microtime(true);
$time = $time_end - $time_start;

echo 'Память -- ' . memory_get_peak_usage(true) . PHP_EOL;
echo 'Общее время -- ' . $time . PHP_EOL;
echo 'Пара -- ' . $time/($count-1) . PHP_EOL;
echo 'пар в секунду -- ' . ($count-1)/$time . PHP_EOL;

$channel->queue_purge('email');
$channel->queue_purge('sms');

die();